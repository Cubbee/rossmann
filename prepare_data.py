import pandas as pd
from sklearn.ensemble.forest import RandomForestRegressor
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.ensemble import ExtraTreesRegressor
from sklearn.svm import SVR
from sklearn.preprocessing import MinMaxScaler
from sklearn import cross_validation
import numpy as np

TRAIN_NAME = 'data/train.csv'
TEST_NAME = 'data/test.csv'
STORE_NAME = 'data/store.csv'

MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']


def month_to_column(comma_separated_months):
    result = {}
    for month in MONTHS:
        result['PromoStart' + month] = 0

    if isinstance(comma_separated_months, str):
        present_months = comma_separated_months.split(',')
        for month in present_months:
            result['PromoStart' + month] = 1

    return result


def preprocess_data(raw_data, store_preprocessed_data):
    binary_state_holidays = pd.get_dummies(raw_data['StateHoliday']).rename(columns={'a':'PublicHoliday', 'b':'EasterHoliday', 'c':'Christmas'}).drop('0', axis=1)
    preprocessed_data = raw_data.drop('StateHoliday', 1).join(binary_state_holidays)

    preprocessed_data['Year'] = preprocessed_data['Date'].apply(lambda x: x.year)
    preprocessed_data['Month'] = preprocessed_data['Date'].apply(lambda x: x.month)
    preprocessed_data['Week'] = preprocessed_data['Date'].apply(lambda x: x.weekofyear)
    preprocessed_data.drop(['Date'], axis=1, inplace=True)

    preprocessed_data = pd.merge(preprocessed_data, store_preprocessed_data, on='Store')
    preprocessed_data.drop('Store', axis=1, inplace=True)

    preprocessed_data['Open'].fillna(1, inplace=True)

    #TODO: smart clean for NaNs
    preprocessed_data.fillna(0, inplace=True)

    # Merge sorts data by Store. For some reason it drops CV quality dramatically. Reshuffle to account for this.
    preprocessed_data = preprocessed_data.reindex(np.random.permutation(preprocessed_data.index))

    # Sort columns
    preprocessed_data.sort_index(axis=1, inplace=True)

    return preprocessed_data

def RMSPE(arr_actuals, arr_predictions):
     assert arr_actuals.shape[0] == arr_predictions.shape[0], 'unequal number of rows'

     # Ignore actuals with a value of 0 for scoring
     arr_predictions = arr_predictions[arr_actuals != 0].astype(float)
     arr_actuals = arr_actuals[arr_actuals != 0].astype(float)

     pct_error = (arr_actuals - arr_predictions) / arr_actuals
     SPE = pct_error**2

     MSPE = np.mean(SPE)

     RMSPE = np.sqrt(MSPE)

     return RMSPE

if __name__ == '__main__':
    store_raw_data = pd.read_csv(STORE_NAME, low_memory=False)

    binary_store_types = pd.get_dummies(store_raw_data['StoreType']).rename(columns={'a':'StoreTypeA', 'b':'StoreTypeB', 'c':'StoreTypeC', 'd':'StoreTypeD'})
    binary_assortments = pd.get_dummies(store_raw_data['Assortment']).rename(columns={'a':'AssortmentBasic', 'b':'AssortmentExtra', 'c':'AssortmentExtended'})
    binary_promo_months = store_raw_data['PromoInterval'].apply(lambda s: pd.Series(month_to_column(s)))

    store_preprocessed_data = store_raw_data.drop(['StoreType', 'Assortment', 'PromoInterval'], 1).join(binary_store_types).join(binary_assortments).join(binary_promo_months)

    train_raw_data = pd.read_csv(TRAIN_NAME, low_memory=False, parse_dates=['Date'])
    train_raw_data = train_raw_data[train_raw_data['Open'] != 0]
    train_preprocessed_data = preprocess_data(train_raw_data, store_preprocessed_data)

    test_raw_data = pd.read_csv(TEST_NAME, low_memory=False, parse_dates=['Date'])
    test_preprocessed_data = preprocess_data(test_raw_data, store_preprocessed_data)

    X = train_preprocessed_data.drop(['Sales', 'Customers'], 1)
    y = train_preprocessed_data['Sales']

    for col in train_preprocessed_data.columns:
        if col not in test_preprocessed_data.columns and col != 'Sales' and col != 'Customers':
            test_preprocessed_data[col] = 0

    #forest = RandomForestRegressor(n_estimators=15, n_jobs=-1, verbose=1)
    #forest = SVR(verbose=4, tol=1e-2, max_iter=1000)
    forest = ExtraTreesRegressor(verbose=4, n_estimators=200, n_jobs=-1, min_samples_leaf=3, min_samples_split=4)
    #forest = GradientBoostingRegressor(verbose=4)
    #preprocessor = MinMaxScaler()
    #X = preprocessor.fit_transform(X, y)

    X_predictions = cross_validation.cross_val_predict(forest, X, y, verbose=1, cv=3)
    print 'RMSPE: ', RMSPE(y.as_matrix(), X_predictions)

    #X_train, X_test, y_train, y_test = cross_validation.train_test_split(X, y, test_size=0.2, random_state=42)

    forest.fit(X, y)

    test_preprocessed_data = test_preprocessed_data.sort_index(axis=1).set_index('Id')
    #test_preprocessed_transformed_data = preprocessor.transform(test_preprocessed_data)
    #predicted_sales = forest.predict(test_preprocessed_transformed_data)

    predicted_sales = forest.predict(test_preprocessed_data)
    result = pd.DataFrame({'Id': test_preprocessed_data.index.values, 'Sales': predicted_sales}).set_index('Id')
    result.sort_index().to_csv('result.csv')

    print('test')



